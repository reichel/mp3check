mp3check (0.8.7-5) unstable; urgency=medium

  * Add comment in debian/watch to explain why the watch file no longer
    works.
  * Add new Vcs-* fields in debian/control.
  * Remove Suggests: mpg321 (instead of replacing it with mpg123) since it
    seems to be completely unused (Closes: #1067879).
  * Update Standards-Version to 4.6.2 (no changes needed).

 -- Joachim Reichel <reichel@debian.org>  Sun, 31 Mar 2024 15:23:44 +0200

mp3check (0.8.7-4) unstable; urgency=medium

  * New maintainer (Closes: #997973).
  * Update Standards-Version to 4.6.1 (no changes needed).
  * Enable "bindnow" for hardening options.
  * Removed outdated Vcs-* fields in debian/control.
  * Fix spelling errors in error messages.
  * Use "https" instead of "http" in various URLs.
  * Refresh and reorder patches.
  * Add lintian override for obsolete-url-in-packaging.

 -- Joachim Reichel <reichel@debian.org>  Sun, 21 Aug 2022 20:09:41 +0200

mp3check (0.8.7-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches/60_bts925782_ftbfs_with_gcc_9.patch: added to fix FTBFS
    with GCC-9. Thanks to Joachim Reichel <joachim.reichel@posteo.de>. (Closes:
    #925782)

 -- David da Silva Polverari <david.polverari@gmail.com>  Thu, 11 Jun 2020 00:33:53 -0500

mp3check (0.8.7-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS; Closes: #865857
    + Instruct dh_auto_build to pass cross compilers.
    + Upstream expects CC to contain a C++ compiler.
    + Add debian/patches/nostrip.patch.

  [ Sandro Tosi ]
  * debian/copyright
    - extend packaging copyright years
  * debian/control
    - bump Standards-Version to 4.2.1 (no changes needed)
    - update Vcs-* fields, pointing to Salsa

 -- Sandro Tosi <morph@debian.org>  Sat, 22 Dec 2018 18:33:01 -0500

mp3check (0.8.7-2) unstable; urgency=low

  * debian/patches/40_bts726068_remove_truncated_last_frame.patch
    - fix to prevent segfaults when the last audio frame is truncated; thanks to
      Xavier Hienne for the report and patch; Closes: #726068, #315497
  * debian/control
    - bump Standards-Version to 3.9.4 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Thu, 17 Oct 2013 00:36:02 +0200

mp3check (0.8.7-1) unstable; urgency=low

  * New upstream release
  * debian/watch
    - updated to use Google Code
  * debian/copyright
    - updated upstream copyright years
  * debian/patches/{15-bts667288-gcc-4.7.patch, 30_bts624138_manpage_typo.patch}
    - removed, merged upstream
  * debian/{control, copyright}
    - updated upstream website location
  * debian/{rules, patches/30_hardening.patch}
    - build using the hardening flags

 -- Sandro Tosi <morph@debian.org>  Sat, 09 Jun 2012 12:02:50 +0200

mp3check (0.8.4-1) unstable; urgency=low

  * New upstream release
  * debian/watch
    - updated
  * debian/control
    - added Vcs-{Git, Browser} fields
    - added ${misc:Depends} to Depends line
    - bump Standards-Version to 3.9.3 (no changes needed)
  * Converted to 3.0 (quilt) source format
  * Converted to debhelper 9 and dh sequencer
  * debian/copyright
    - updated to DEP5 and to new upstream release
  * debian/patches/20_use_destdir_to_install.patch
    - install using DESTDIR variable
  * debian/rules
    - upstream doesn't ship doc anymore
    - install the manpage (no longer done by upstream Makefile)
  * debian/patches/15-bts667288-gcc-4.7.patch
    - fix FTBFS with GCC-4.7 (due to uncoordinated upload by gcc "maintainer");
      thanks to Jari Aalto for the patch; Closes: #667288
  * debian/patches/30_bts624138_manpage_typo.patch
    - fix a typo in the manpage; thanks to Reuben Thomas for the report;
      Closes: #624138

 -- Sandro Tosi <morph@debian.org>  Sat, 12 May 2012 16:08:03 +0200

mp3check (0.8.3-2) unstable; urgency=low

  * debian/control
    - updated my email address
    - bump Standards-Version to 3.8.1
      + added debian/README.source
  * debian/copyright
    - added explicit packaging copyright section, adding my contributions
    - link to versioned GPL-2 license file
  * debian/patches/*.dpatch
    - updated my email address
  * debian/rules
    - merged 'rm' into dh_clean call

 -- Sandro Tosi <morph@debian.org>  Fri, 22 May 2009 18:47:05 +0200

mp3check (0.8.3-1) unstable; urgency=low

  * Adopting package (Closes: #465887)
  * Aknowledge NMU (Closes: #399047, #395451)
  * New upstream release (Closes: #437399)
  * debian/control
    - set myself as maintainer
    - bump Standards-Version to 3.7.3
    - added Homepage field
    - little changes in short description
    - removed tailing spaces from long description
    - added dpatch build-dep
  * debian/watch
    - added
  * debian/rules
    - removed DH_COMPAT
    - commented DH_VERBOSE
    - no longer ignores clean error
    - removedtemplate header
    - integrated debian/docs
    - removed unneeded/commented dh_* calls
    - added dpatch stuff
    - updated doc files installation
  * debian/compat
    - added
  * debian/docs
    - deleted
  * debian/dirs
    - deleted
  * debian/copyright
    - indented upstream author with 4 spaces
    - added upstrem author email address
    - added copyright notices (previous it was used as license)
    - added a real license note
  * debian/patches/01_restore_pristine_code.dpatch
    - added to remove direct upstream code changes
    - edited for 0.8.3 release
  * debian/patches/10_bts450532-455727_manpage_fixes.dpatch
    - added to fix manpage (Closes: #450532, #455727)

 -- Sandro Tosi <matrixhasu@gmail.com>  Thu, 20 Mar 2008 23:38:42 +0100

mp3check (0.8.0-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fixed protection of tags id3. Patch by Tobias Frost <tobi@coldtobi.de>.
    (Closes: #399047)
  * Built with gcc4, updates from libstdc++5 to libstdc++6 (Closes: #395451).
  * Updated compat to 5.
  * Bumped standards-version to 3.7.2, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Tue, 28 Nov 2006 14:57:12 +0100

mp3check (0.8.0-4) unstable; urgency=low

  * Apply patch to allow mp3check to compile with gcc 3.4. (Closes: #260651)

 -- James Morrison <phython@debian.org>  Sun, 25 Jul 2004 13:38:13 -0700

mp3check (0.8.0-3) unstable; urgency=low

  * Don't write based the end of memory. (Closes: #227977)

 -- James Morrison <phython@debian.org>  Tue, 27 Jan 2004 20:15:07 -0500

mp3check (0.8.0-2) unstable; urgency=low

  * Fix build issues on m68k, alpha and ia64.

 -- James Morrison <phython@debian.org>  Mon, 27 Jan 2003 19:24:27 -0500

mp3check (0.8.0-1) unstable; urgency=low

  * Acknowledge NMU.  (Closes: #143862)
  * Limit the description lines in debian/control to 80 characters.
  (Closes: 144750)

 -- James Morrison <phython@debian.org>  Sat, 18 Jan 2003 15:36:44 -0500

mp3check (0.7.3-4.1) unstable; urgency=high

  * Put the binary back in. Closes: #143862 (NMU; patch in bug report)
  * s/dh_installmanpages/dh_installman/; so it installs the man page instead
    of skipping it.

 -- Joey Hess <joeyh@debian.org>  Sat, 27 Apr 2002 11:57:07 -0400

mp3check (0.7.3-4) unstable; urgency=low

  * Fix Makefile.in to define CXX and CC.
  * mp3check.cc: use namespace std and include <iostream>. (Closes: #143252)

 -- James Morrison <phython@debian.org>  Wed, 17 Apr 2002 09:51:24 -0400

mp3check (0.7.3-3) unstable; urgency=low

  * Include autoconf in the build dependencies. (Closes: #143090)

 -- James Morrison <phython@debian.org>  Tue, 16 Apr 2002 00:11:41 -0400

mp3check (0.7.3-2) unstable; urgency=low

  * Run autoconf before the configure script. (Closes: #142931)

 -- James Morrison <phython@debian.org>  Mon, 15 Apr 2002 11:42:58 -0400

mp3check (0.7.3-1) unstable; urgency=low

  * New upstream version. (Closes: #140676).
  * New maintainer (Closes: #142840)
  * Use asprintf instead of vasprintf for exception handling. (Closes: #104982)
  * Updated upstream download site.

 -- James Morrison <phython@debian.org>  Sun, 14 Apr 2002 09:41:33 -0400

mp3check (0.5.2-1) unstable; urgency=low

  * Initial Release.
  * Adjusted Makefile to fix $(DESTDIR) problems.

 -- Klaus Kettner <kk@debian.org>  Sat,  2 Sep 2000 22:26:01 +0200

